import {Row, Col} from 'antd';

import {Form, Input, Button, Checkbox } from 'antd';

const DemoBox=props=><p className={`height-${props.value}`}>{props.children}</p>;



const layout={
	labelCol:{span:8},
	wrapperCol:{span:8},
};

const tailLayout={
	wrapperCol:{offset:8,span:16},
};


const onFinish=values=>{
	console.log('Success',values);
};

const onFinishFailed=errorInfo=>{
	console.log('Failed', errorInfo);
};



const contenedorPrincipal={backgroundColor:'#A9A9A9',height:'100vh'};

const columnaPrincipal={backgroundColor:'white',paddingTop:'1rem'};

function ParaForm() {
  return <>
    <Row justify="center" align="middle" style={contenedorPrincipal}>
        <Col span={12} style={columnaPrincipal}>
            <DemoBox value={50}>
                <Form
                  {...layout}
                  name="basic"
                  initialValues={{remember:true}}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  >
                    <h2 style={{textAlign:'center'}}>Login</h2>
                    <Form.Item
                         label="Email"
                         name="email"
                         rules={[{type:'email', required:true, message:'Please input your username!'}]}
                         >
                         <Input/>
                     </Form.Item>


                     <Form.Item
                         label="Password"
                         name="password"
                         rules={[{required:true,message:'please input your password!'}]}
                         >
                         <Input.Password/>

                     </Form.Item>


                     <Form.Item {...tailLayout}>
                         <Button type="primary" htmlType="submit">
                            Iniciar Sesión
                        </Button>
                    </Form.Item>
                </Form>
            </DemoBox>
        </Col>
    </Row>  

  </>
}

export default ParaForm;