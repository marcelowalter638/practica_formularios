webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");




var _jsxFileName = "G:\\practicasTutoriales\\next\\practicaFormularios\\pages\\index.js",
    _this = undefined;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }




var DemoBox = function DemoBox(props) {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])("p", {
    className: "height-".concat(props.value),
    children: props.children
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 5,
    columnNumber: 22
  }, _this);
};

_c = DemoBox;
var layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 8
  }
};
var tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
};

var onFinish = function onFinish(values) {
  console.log('Success', values);
};

var onFinishFailed = function onFinishFailed(errorInfo) {
  console.log('Failed', errorInfo);
};

var contenedorPrincipal = {
  backgroundColor: '#A9A9A9',
  height: '100vh'
};
var columnaPrincipal = {
  backgroundColor: 'white',
  paddingTop: '1rem'
};

function ParaForm() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Row"], {
      justify: "center",
      align: "middle",
      style: contenedorPrincipal,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        span: 12,
        style: columnaPrincipal,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(DemoBox, {
          value: 50,
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"], _objectSpread(_objectSpread({}, layout), {}, {
            name: "basic",
            initialValues: {
              remember: true
            },
            onFinish: onFinish,
            onFinishFailed: onFinishFailed,
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])("h2", {
              style: {
                textAlign: 'center'
              },
              children: "Iniciar Sesi\xF3n"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 45,
              columnNumber: 21
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
              label: "Email",
              name: "email",
              rules: [{
                type: 'email',
                required: true,
                message: 'Please input your username!'
              }],
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"], {}, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 51,
                columnNumber: 26
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 46,
              columnNumber: 21
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
              label: "Password",
              name: "password",
              rules: [{
                required: true,
                message: 'please input your password!'
              }],
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"].Password, {}, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 60,
                columnNumber: 26
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 55,
              columnNumber: 22
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, _objectSpread(_objectSpread({}, tailLayout), {}, {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Button"], {
                type: "primary",
                htmlType: "submit",
                children: "Submit"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 66,
                columnNumber: 26
              }, this)
            }), void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 65,
              columnNumber: 22
            }, this)]
          }), void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 38,
            columnNumber: 17
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 37,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 5
    }, this)
  }, void 0, false);
}

_c2 = ParaForm;
/* harmony default export */ __webpack_exports__["default"] = (ParaForm);

var _c, _c2;

$RefreshReg$(_c, "DemoBox");
$RefreshReg$(_c2, "ParaForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiRGVtb0JveCIsInByb3BzIiwidmFsdWUiLCJjaGlsZHJlbiIsImxheW91dCIsImxhYmVsQ29sIiwic3BhbiIsIndyYXBwZXJDb2wiLCJ0YWlsTGF5b3V0Iiwib2Zmc2V0Iiwib25GaW5pc2giLCJ2YWx1ZXMiLCJjb25zb2xlIiwibG9nIiwib25GaW5pc2hGYWlsZWQiLCJlcnJvckluZm8iLCJjb250ZW5lZG9yUHJpbmNpcGFsIiwiYmFja2dyb3VuZENvbG9yIiwiaGVpZ2h0IiwiY29sdW1uYVByaW5jaXBhbCIsInBhZGRpbmdUb3AiLCJQYXJhRm9ybSIsInJlbWVtYmVyIiwidGV4dEFsaWduIiwidHlwZSIsInJlcXVpcmVkIiwibWVzc2FnZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBOztBQUVBLElBQU1BLE9BQU8sR0FBQyxTQUFSQSxPQUFRLENBQUFDLEtBQUs7QUFBQSxzQkFBRTtBQUFHLGFBQVMsbUJBQVlBLEtBQUssQ0FBQ0MsS0FBbEIsQ0FBWjtBQUFBLGNBQXdDRCxLQUFLLENBQUNFO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FBRjtBQUFBLENBQW5COztLQUFNSCxPO0FBSU4sSUFBTUksTUFBTSxHQUFDO0FBQ1pDLFVBQVEsRUFBQztBQUFDQyxRQUFJLEVBQUM7QUFBTixHQURHO0FBRVpDLFlBQVUsRUFBQztBQUFDRCxRQUFJLEVBQUM7QUFBTjtBQUZDLENBQWI7QUFLQSxJQUFNRSxVQUFVLEdBQUM7QUFDaEJELFlBQVUsRUFBQztBQUFDRSxVQUFNLEVBQUMsQ0FBUjtBQUFVSCxRQUFJLEVBQUM7QUFBZjtBQURLLENBQWpCOztBQUtBLElBQU1JLFFBQVEsR0FBQyxTQUFUQSxRQUFTLENBQUFDLE1BQU0sRUFBRTtBQUN0QkMsU0FBTyxDQUFDQyxHQUFSLENBQVksU0FBWixFQUFzQkYsTUFBdEI7QUFDQSxDQUZEOztBQUlBLElBQU1HLGNBQWMsR0FBQyxTQUFmQSxjQUFlLENBQUFDLFNBQVMsRUFBRTtBQUMvQkgsU0FBTyxDQUFDQyxHQUFSLENBQVksUUFBWixFQUFzQkUsU0FBdEI7QUFDQSxDQUZEOztBQU1BLElBQU1DLG1CQUFtQixHQUFDO0FBQUNDLGlCQUFlLEVBQUMsU0FBakI7QUFBMkJDLFFBQU0sRUFBQztBQUFsQyxDQUExQjtBQUVBLElBQU1DLGdCQUFnQixHQUFDO0FBQUNGLGlCQUFlLEVBQUMsT0FBakI7QUFBeUJHLFlBQVUsRUFBQztBQUFwQyxDQUF2Qjs7QUFFQSxTQUFTQyxRQUFULEdBQW9CO0FBQ2xCLHNCQUFPO0FBQUEsMkJBQ0wscUVBQUMsd0NBQUQ7QUFBSyxhQUFPLEVBQUMsUUFBYjtBQUFzQixXQUFLLEVBQUMsUUFBNUI7QUFBcUMsV0FBSyxFQUFFTCxtQkFBNUM7QUFBQSw2QkFDSSxxRUFBQyx3Q0FBRDtBQUFLLFlBQUksRUFBRSxFQUFYO0FBQWUsYUFBSyxFQUFFRyxnQkFBdEI7QUFBQSwrQkFDSSxxRUFBQyxPQUFEO0FBQVMsZUFBSyxFQUFFLEVBQWhCO0FBQUEsaUNBQ0kscUVBQUMseUNBQUQsa0NBQ01mLE1BRE47QUFFRSxnQkFBSSxFQUFDLE9BRlA7QUFHRSx5QkFBYSxFQUFFO0FBQUNrQixzQkFBUSxFQUFDO0FBQVYsYUFIakI7QUFJRSxvQkFBUSxFQUFFWixRQUpaO0FBS0UsMEJBQWMsRUFBRUksY0FMbEI7QUFBQSxvQ0FPSTtBQUFJLG1CQUFLLEVBQUU7QUFBQ1MseUJBQVMsRUFBQztBQUFYLGVBQVg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBUEosZUFRSSxxRUFBQyx5Q0FBRCxDQUFNLElBQU47QUFDSyxtQkFBSyxFQUFDLE9BRFg7QUFFSyxrQkFBSSxFQUFDLE9BRlY7QUFHSyxtQkFBSyxFQUFFLENBQUM7QUFBQ0Msb0JBQUksRUFBQyxPQUFOO0FBQWVDLHdCQUFRLEVBQUMsSUFBeEI7QUFBOEJDLHVCQUFPLEVBQUM7QUFBdEMsZUFBRCxDQUhaO0FBQUEscUNBS0sscUVBQUMsMENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxMO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBUkosZUFpQksscUVBQUMseUNBQUQsQ0FBTSxJQUFOO0FBQ0ksbUJBQUssRUFBQyxVQURWO0FBRUksa0JBQUksRUFBQyxVQUZUO0FBR0ksbUJBQUssRUFBRSxDQUFDO0FBQUNELHdCQUFRLEVBQUMsSUFBVjtBQUFlQyx1QkFBTyxFQUFDO0FBQXZCLGVBQUQsQ0FIWDtBQUFBLHFDQUtJLHFFQUFDLDBDQUFELENBQU8sUUFBUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEo7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFqQkwsZUEyQksscUVBQUMseUNBQUQsQ0FBTSxJQUFOLGtDQUFlbEIsVUFBZjtBQUFBLHFDQUNJLHFFQUFDLDJDQUFEO0FBQVEsb0JBQUksRUFBQyxTQUFiO0FBQXVCLHdCQUFRLEVBQUMsUUFBaEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQTNCTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESyxtQkFBUDtBQTBDRDs7TUEzQ1FhLFE7QUE2Q01BLHVFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4Ljk1MDM3YmRkMGQxYWVhMDlhM2RkLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1JvdywgQ29sfSBmcm9tICdhbnRkJztcclxuXHJcbmltcG9ydCB7Rm9ybSwgSW5wdXQsIEJ1dHRvbiwgQ2hlY2tib3ggfSBmcm9tICdhbnRkJztcclxuXHJcbmNvbnN0IERlbW9Cb3g9cHJvcHM9PjxwIGNsYXNzTmFtZT17YGhlaWdodC0ke3Byb3BzLnZhbHVlfWB9Pntwcm9wcy5jaGlsZHJlbn08L3A+O1xyXG5cclxuXHJcblxyXG5jb25zdCBsYXlvdXQ9e1xyXG5cdGxhYmVsQ29sOntzcGFuOjh9LFxyXG5cdHdyYXBwZXJDb2w6e3NwYW46OH0sXHJcbn07XHJcblxyXG5jb25zdCB0YWlsTGF5b3V0PXtcclxuXHR3cmFwcGVyQ29sOntvZmZzZXQ6OCxzcGFuOjE2fSxcclxufTtcclxuXHJcblxyXG5jb25zdCBvbkZpbmlzaD12YWx1ZXM9PntcclxuXHRjb25zb2xlLmxvZygnU3VjY2VzcycsdmFsdWVzKTtcclxufTtcclxuXHJcbmNvbnN0IG9uRmluaXNoRmFpbGVkPWVycm9ySW5mbz0+e1xyXG5cdGNvbnNvbGUubG9nKCdGYWlsZWQnLCBlcnJvckluZm8pO1xyXG59O1xyXG5cclxuXHJcblxyXG5jb25zdCBjb250ZW5lZG9yUHJpbmNpcGFsPXtiYWNrZ3JvdW5kQ29sb3I6JyNBOUE5QTknLGhlaWdodDonMTAwdmgnfTtcclxuXHJcbmNvbnN0IGNvbHVtbmFQcmluY2lwYWw9e2JhY2tncm91bmRDb2xvcjond2hpdGUnLHBhZGRpbmdUb3A6JzFyZW0nfTtcclxuXHJcbmZ1bmN0aW9uIFBhcmFGb3JtKCkge1xyXG4gIHJldHVybiA8PlxyXG4gICAgPFJvdyBqdXN0aWZ5PVwiY2VudGVyXCIgYWxpZ249XCJtaWRkbGVcIiBzdHlsZT17Y29udGVuZWRvclByaW5jaXBhbH0+XHJcbiAgICAgICAgPENvbCBzcGFuPXsxMn0gc3R5bGU9e2NvbHVtbmFQcmluY2lwYWx9PlxyXG4gICAgICAgICAgICA8RGVtb0JveCB2YWx1ZT17NTB9PlxyXG4gICAgICAgICAgICAgICAgPEZvcm1cclxuICAgICAgICAgICAgICAgICAgey4uLmxheW91dH1cclxuICAgICAgICAgICAgICAgICAgbmFtZT1cImJhc2ljXCJcclxuICAgICAgICAgICAgICAgICAgaW5pdGlhbFZhbHVlcz17e3JlbWVtYmVyOnRydWV9fVxyXG4gICAgICAgICAgICAgICAgICBvbkZpbmlzaD17b25GaW5pc2h9XHJcbiAgICAgICAgICAgICAgICAgIG9uRmluaXNoRmFpbGVkPXtvbkZpbmlzaEZhaWxlZH1cclxuICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoMiBzdHlsZT17e3RleHRBbGlnbjonY2VudGVyJ319PkluaWNpYXIgU2VzacOzbjwvaDI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9XCJFbWFpbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwiZW1haWxcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgcnVsZXM9e1t7dHlwZTonZW1haWwnLCByZXF1aXJlZDp0cnVlLCBtZXNzYWdlOidQbGVhc2UgaW5wdXQgeW91ciB1c2VybmFtZSEnfV19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICA8SW5wdXQvPlxyXG4gICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uSXRlbT5cclxuXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICA8Rm9ybS5JdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD1cIlBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBydWxlcz17W3tyZXF1aXJlZDp0cnVlLG1lc3NhZ2U6J3BsZWFzZSBpbnB1dCB5b3VyIHBhc3N3b3JkISd9XX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgIDxJbnB1dC5QYXNzd29yZC8+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uSXRlbT5cclxuXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICA8Rm9ybS5JdGVtIHsuLi50YWlsTGF5b3V0fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gdHlwZT1cInByaW1hcnlcIiBodG1sVHlwZT1cInN1Ym1pdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgU3VibWl0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvRm9ybS5JdGVtPlxyXG4gICAgICAgICAgICAgICAgPC9Gb3JtPlxyXG4gICAgICAgICAgICA8L0RlbW9Cb3g+XHJcbiAgICAgICAgPC9Db2w+XHJcbiAgICA8L1Jvdz4gIFxyXG5cclxuICA8Lz5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUGFyYUZvcm07Il0sInNvdXJjZVJvb3QiOiIifQ==