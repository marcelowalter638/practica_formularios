webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");




var _jsxFileName = "G:\\practicasTutoriales\\next\\practicaFormularios\\pages\\index.js",
    _this = undefined;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }




var DemoBox = function DemoBox(props) {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])("p", {
    className: "height-".concat(props.value),
    children: props.children
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 5,
    columnNumber: 22
  }, _this);
};

_c = DemoBox;
var layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 8
  }
};
var tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
};

var onFinish = function onFinish(values) {
  console.log('Success', values);
};

var onFinishFailed = function onFinishFailed(errorInfo) {
  console.log('Failed', errorInfo);
};

function ParaForm() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Row"], {
      justify: "center",
      style: {
        backgroundColor: 'red',
        height: '100vh'
      },
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        span: 12,
        style: {
          backgroundColor: 'orange'
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(DemoBox, {
          value: 50,
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"], _objectSpread(_objectSpread({}, layout), {}, {
            name: "basic",
            initialValues: {
              remember: true
            },
            onFinish: onFinish,
            onFinishFailed: onFinishFailed,
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
              label: "Email",
              name: "email",
              rules: [{
                type: 'email',
                required: true,
                message: 'Please input your username!'
              }],
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"], {}, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 49,
                columnNumber: 26
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 44,
              columnNumber: 23
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
              label: "Password",
              name: "password",
              rules: [{
                required: true,
                message: 'please input your password!'
              }],
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"].Password, {}, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 58,
                columnNumber: 26
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 53,
              columnNumber: 22
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, _objectSpread(_objectSpread({}, tailLayout), {}, {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Button"], {
                type: "primary",
                htmlType: "submit",
                children: "Submit"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 64,
                columnNumber: 26
              }, this)
            }), void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 63,
              columnNumber: 22
            }, this)]
          }), void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 37,
            columnNumber: 17
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 36,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 5
    }, this)
  }, void 0, false);
}

_c2 = ParaForm;
/* harmony default export */ __webpack_exports__["default"] = (ParaForm);

var _c, _c2;

$RefreshReg$(_c, "DemoBox");
$RefreshReg$(_c2, "ParaForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiRGVtb0JveCIsInByb3BzIiwidmFsdWUiLCJjaGlsZHJlbiIsImxheW91dCIsImxhYmVsQ29sIiwic3BhbiIsIndyYXBwZXJDb2wiLCJ0YWlsTGF5b3V0Iiwib2Zmc2V0Iiwib25GaW5pc2giLCJ2YWx1ZXMiLCJjb25zb2xlIiwibG9nIiwib25GaW5pc2hGYWlsZWQiLCJlcnJvckluZm8iLCJQYXJhRm9ybSIsImJhY2tncm91bmRDb2xvciIsImhlaWdodCIsInJlbWVtYmVyIiwidHlwZSIsInJlcXVpcmVkIiwibWVzc2FnZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBOztBQUVBLElBQU1BLE9BQU8sR0FBQyxTQUFSQSxPQUFRLENBQUFDLEtBQUs7QUFBQSxzQkFBRTtBQUFHLGFBQVMsbUJBQVlBLEtBQUssQ0FBQ0MsS0FBbEIsQ0FBWjtBQUFBLGNBQXdDRCxLQUFLLENBQUNFO0FBQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FBRjtBQUFBLENBQW5COztLQUFNSCxPO0FBSU4sSUFBTUksTUFBTSxHQUFDO0FBQ1pDLFVBQVEsRUFBQztBQUFDQyxRQUFJLEVBQUM7QUFBTixHQURHO0FBRVpDLFlBQVUsRUFBQztBQUFDRCxRQUFJLEVBQUM7QUFBTjtBQUZDLENBQWI7QUFLQSxJQUFNRSxVQUFVLEdBQUM7QUFDaEJELFlBQVUsRUFBQztBQUFDRSxVQUFNLEVBQUMsQ0FBUjtBQUFVSCxRQUFJLEVBQUM7QUFBZjtBQURLLENBQWpCOztBQUtBLElBQU1JLFFBQVEsR0FBQyxTQUFUQSxRQUFTLENBQUFDLE1BQU0sRUFBRTtBQUN0QkMsU0FBTyxDQUFDQyxHQUFSLENBQVksU0FBWixFQUFzQkYsTUFBdEI7QUFDQSxDQUZEOztBQUlBLElBQU1HLGNBQWMsR0FBQyxTQUFmQSxjQUFlLENBQUFDLFNBQVMsRUFBRTtBQUMvQkgsU0FBTyxDQUFDQyxHQUFSLENBQVksUUFBWixFQUFzQkUsU0FBdEI7QUFDQSxDQUZEOztBQVNBLFNBQVNDLFFBQVQsR0FBb0I7QUFDbEIsc0JBQU87QUFBQSwyQkFDTCxxRUFBQyx3Q0FBRDtBQUFLLGFBQU8sRUFBQyxRQUFiO0FBQXNCLFdBQUssRUFBRTtBQUFDQyx1QkFBZSxFQUFDLEtBQWpCO0FBQXVCQyxjQUFNLEVBQUM7QUFBOUIsT0FBN0I7QUFBQSw2QkFDSSxxRUFBQyx3Q0FBRDtBQUFLLFlBQUksRUFBRSxFQUFYO0FBQWUsYUFBSyxFQUFFO0FBQUNELHlCQUFlLEVBQUM7QUFBakIsU0FBdEI7QUFBQSwrQkFDSSxxRUFBQyxPQUFEO0FBQVMsZUFBSyxFQUFFLEVBQWhCO0FBQUEsaUNBQ0kscUVBQUMseUNBQUQsa0NBQ01iLE1BRE47QUFFRSxnQkFBSSxFQUFDLE9BRlA7QUFHRSx5QkFBYSxFQUFFO0FBQUNlLHNCQUFRLEVBQUM7QUFBVixhQUhqQjtBQUlFLG9CQUFRLEVBQUVULFFBSlo7QUFLRSwwQkFBYyxFQUFFSSxjQUxsQjtBQUFBLG9DQU9NLHFFQUFDLHlDQUFELENBQU0sSUFBTjtBQUNHLG1CQUFLLEVBQUMsT0FEVDtBQUVHLGtCQUFJLEVBQUMsT0FGUjtBQUdHLG1CQUFLLEVBQUUsQ0FBQztBQUFDTSxvQkFBSSxFQUFDLE9BQU47QUFBZUMsd0JBQVEsRUFBQyxJQUF4QjtBQUE4QkMsdUJBQU8sRUFBQztBQUF0QyxlQUFELENBSFY7QUFBQSxxQ0FLRyxxRUFBQywwQ0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEg7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFQTixlQWdCSyxxRUFBQyx5Q0FBRCxDQUFNLElBQU47QUFDSSxtQkFBSyxFQUFDLFVBRFY7QUFFSSxrQkFBSSxFQUFDLFVBRlQ7QUFHSSxtQkFBSyxFQUFFLENBQUM7QUFBQ0Qsd0JBQVEsRUFBQyxJQUFWO0FBQWVDLHVCQUFPLEVBQUM7QUFBdkIsZUFBRCxDQUhYO0FBQUEscUNBS0kscUVBQUMsMENBQUQsQ0FBTyxRQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMSjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQWhCTCxlQTBCSyxxRUFBQyx5Q0FBRCxDQUFNLElBQU4sa0NBQWVkLFVBQWY7QUFBQSxxQ0FDSSxxRUFBQywyQ0FBRDtBQUFRLG9CQUFJLEVBQUMsU0FBYjtBQUF1Qix3QkFBUSxFQUFDLFFBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkExQkw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREssbUJBQVA7QUF5Q0Q7O01BMUNRUSxRO0FBNENNQSx1RUFBZiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC5iNTg5ZTdlYjBhNWNiZjIzMmNlMS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSb3csIENvbH0gZnJvbSAnYW50ZCc7XHJcblxyXG5pbXBvcnQge0Zvcm0sIElucHV0LCBCdXR0b24sIENoZWNrYm94IH0gZnJvbSAnYW50ZCc7XHJcblxyXG5jb25zdCBEZW1vQm94PXByb3BzPT48cCBjbGFzc05hbWU9e2BoZWlnaHQtJHtwcm9wcy52YWx1ZX1gfT57cHJvcHMuY2hpbGRyZW59PC9wPjtcclxuXHJcblxyXG5cclxuY29uc3QgbGF5b3V0PXtcclxuXHRsYWJlbENvbDp7c3Bhbjo4fSxcclxuXHR3cmFwcGVyQ29sOntzcGFuOjh9LFxyXG59O1xyXG5cclxuY29uc3QgdGFpbExheW91dD17XHJcblx0d3JhcHBlckNvbDp7b2Zmc2V0Ojgsc3BhbjoxNn0sXHJcbn07XHJcblxyXG5cclxuY29uc3Qgb25GaW5pc2g9dmFsdWVzPT57XHJcblx0Y29uc29sZS5sb2coJ1N1Y2Nlc3MnLHZhbHVlcyk7XHJcbn07XHJcblxyXG5jb25zdCBvbkZpbmlzaEZhaWxlZD1lcnJvckluZm89PntcclxuXHRjb25zb2xlLmxvZygnRmFpbGVkJywgZXJyb3JJbmZvKTtcclxufTtcclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuZnVuY3Rpb24gUGFyYUZvcm0oKSB7XHJcbiAgcmV0dXJuIDw+XHJcbiAgICA8Um93IGp1c3RpZnk9XCJjZW50ZXJcIiBzdHlsZT17e2JhY2tncm91bmRDb2xvcjoncmVkJyxoZWlnaHQ6JzEwMHZoJ319PlxyXG4gICAgICAgIDxDb2wgc3Bhbj17MTJ9IHN0eWxlPXt7YmFja2dyb3VuZENvbG9yOidvcmFuZ2UnfX0+XHJcbiAgICAgICAgICAgIDxEZW1vQm94IHZhbHVlPXs1MH0+XHJcbiAgICAgICAgICAgICAgICA8Rm9ybVxyXG4gICAgICAgICAgICAgICAgICB7Li4ubGF5b3V0fVxyXG4gICAgICAgICAgICAgICAgICBuYW1lPVwiYmFzaWNcIlxyXG4gICAgICAgICAgICAgICAgICBpbml0aWFsVmFsdWVzPXt7cmVtZW1iZXI6dHJ1ZX19XHJcbiAgICAgICAgICAgICAgICAgIG9uRmluaXNoPXtvbkZpbmlzaH1cclxuICAgICAgICAgICAgICAgICAgb25GaW5pc2hGYWlsZWQ9e29uRmluaXNoRmFpbGVkfVxyXG4gICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8Rm9ybS5JdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD1cIkVtYWlsXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJlbWFpbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBydWxlcz17W3t0eXBlOidlbWFpbCcsIHJlcXVpcmVkOnRydWUsIG1lc3NhZ2U6J1BsZWFzZSBpbnB1dCB5b3VyIHVzZXJuYW1lISd9XX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgIDxJbnB1dC8+XHJcbiAgICAgICAgICAgICAgICAgICAgIDwvRm9ybS5JdGVtPlxyXG5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgIDxGb3JtLkl0ZW1cclxuICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPVwiUGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgbmFtZT1cInBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGVzPXtbe3JlcXVpcmVkOnRydWUsbWVzc2FnZToncGxlYXNlIGlucHV0IHlvdXIgcGFzc3dvcmQhJ31dfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgPElucHV0LlBhc3N3b3JkLz5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgIDwvRm9ybS5JdGVtPlxyXG5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgIDxGb3JtLkl0ZW0gey4uLnRhaWxMYXlvdXR9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiB0eXBlPVwicHJpbWFyeVwiIGh0bWxUeXBlPVwic3VibWl0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBTdWJtaXRcclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkl0ZW0+XHJcbiAgICAgICAgICAgICAgICA8L0Zvcm0+XHJcbiAgICAgICAgICAgIDwvRGVtb0JveD5cclxuICAgICAgICA8L0NvbD5cclxuICAgIDwvUm93PiAgXHJcblxyXG4gIDwvPlxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBQYXJhRm9ybTsiXSwic291cmNlUm9vdCI6IiJ9