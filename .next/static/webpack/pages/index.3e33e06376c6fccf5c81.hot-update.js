webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");



var _jsxFileName = "G:\\practicasTutoriales\\next\\practicaFormularios\\pages\\index.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }


var layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
};
var tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
};

var onFinish = function onFinish(values) {
  console.log('Success', values);
};

var onFinishFailed = function onFinishFailed(errorInfo) {
  console.log('Failed', errorInfo);
};

function ParaForm() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"], _objectSpread(_objectSpread({}, layout), {}, {
      name: "basic",
      initialValues: {
        remember: true
      },
      onFinish: onFinish,
      onFinishFailed: onFinishFailed,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
        label: "Username",
        name: "username",
        rules: [{
          required: true,
          message: 'Please input your username!'
        }],
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"], {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 42,
          columnNumber: 6
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 5
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
        label: "Password",
        name: "password",
        rules: [{
          required: true,
          message: 'please input your password!'
        }],
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"].Password, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 51,
          columnNumber: 6
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 46,
        columnNumber: 5
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, _objectSpread(_objectSpread({}, tailLayout), {}, {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Button"], {
          type: "primary",
          htmlType: "submit",
          children: "Submit"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 57,
          columnNumber: 6
        }, this)
      }), void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 5
      }, this)]
    }), void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 4
    }, this)
  }, void 0, false);
}

_c = ParaForm;
/* harmony default export */ __webpack_exports__["default"] = (ParaForm);

var _c;

$RefreshReg$(_c, "ParaForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsibGF5b3V0IiwibGFiZWxDb2wiLCJzcGFuIiwid3JhcHBlckNvbCIsInRhaWxMYXlvdXQiLCJvZmZzZXQiLCJvbkZpbmlzaCIsInZhbHVlcyIsImNvbnNvbGUiLCJsb2ciLCJvbkZpbmlzaEZhaWxlZCIsImVycm9ySW5mbyIsIlBhcmFGb3JtIiwicmVtZW1iZXIiLCJyZXF1aXJlZCIsIm1lc3NhZ2UiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBR0EsSUFBTUEsTUFBTSxHQUFDO0FBQ1pDLFVBQVEsRUFBQztBQUFDQyxRQUFJLEVBQUM7QUFBTixHQURHO0FBRVpDLFlBQVUsRUFBQztBQUFDRCxRQUFJLEVBQUM7QUFBTjtBQUZDLENBQWI7QUFLQSxJQUFNRSxVQUFVLEdBQUM7QUFDaEJELFlBQVUsRUFBQztBQUFDRSxVQUFNLEVBQUMsQ0FBUjtBQUFVSCxRQUFJLEVBQUM7QUFBZjtBQURLLENBQWpCOztBQUtBLElBQU1JLFFBQVEsR0FBQyxTQUFUQSxRQUFTLENBQUFDLE1BQU0sRUFBRTtBQUN0QkMsU0FBTyxDQUFDQyxHQUFSLENBQVksU0FBWixFQUFzQkYsTUFBdEI7QUFDQSxDQUZEOztBQUlBLElBQU1HLGNBQWMsR0FBQyxTQUFmQSxjQUFlLENBQUFDLFNBQVMsRUFBRTtBQUMvQkgsU0FBTyxDQUFDQyxHQUFSLENBQVksUUFBWixFQUFzQkUsU0FBdEI7QUFDQSxDQUZEOztBQVNBLFNBQVNDLFFBQVQsR0FBb0I7QUFDbEIsc0JBQU87QUFBQSwyQkFFTixxRUFBQyx5Q0FBRCxrQ0FDS1osTUFETDtBQUVDLFVBQUksRUFBQyxPQUZOO0FBR0MsbUJBQWEsRUFBRTtBQUFDYSxnQkFBUSxFQUFDO0FBQVYsT0FIaEI7QUFJQyxjQUFRLEVBQUVQLFFBSlg7QUFLQyxvQkFBYyxFQUFFSSxjQUxqQjtBQUFBLDhCQU9DLHFFQUFDLHlDQUFELENBQU0sSUFBTjtBQUNDLGFBQUssRUFBQyxVQURQO0FBRUMsWUFBSSxFQUFDLFVBRk47QUFHQyxhQUFLLEVBQUUsQ0FBQztBQUFDSSxrQkFBUSxFQUFDLElBQVY7QUFBZ0JDLGlCQUFPLEVBQUM7QUFBeEIsU0FBRCxDQUhSO0FBQUEsK0JBS0MscUVBQUMsMENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxEO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FQRCxlQWdCQyxxRUFBQyx5Q0FBRCxDQUFNLElBQU47QUFDQyxhQUFLLEVBQUMsVUFEUDtBQUVDLFlBQUksRUFBQyxVQUZOO0FBR0MsYUFBSyxFQUFFLENBQUM7QUFBQ0Qsa0JBQVEsRUFBQyxJQUFWO0FBQWVDLGlCQUFPLEVBQUM7QUFBdkIsU0FBRCxDQUhSO0FBQUEsK0JBS0MscUVBQUMsMENBQUQsQ0FBTyxRQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBaEJELGVBMEJDLHFFQUFDLHlDQUFELENBQU0sSUFBTixrQ0FBZVgsVUFBZjtBQUFBLCtCQUNDLHFFQUFDLDJDQUFEO0FBQVEsY0FBSSxFQUFDLFNBQWI7QUFBdUIsa0JBQVEsRUFBQyxRQUFoQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUREO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0ExQkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRk0sbUJBQVA7QUFxQ0Q7O0tBdENRUSxRO0FBd0NNQSx1RUFBZiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC4zZTMzZTA2Mzc2YzZmY2NmNWM4MS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtGb3JtLCBJbnB1dCwgQnV0dG9uLCBDaGVja2JveCB9IGZyb20gJ2FudGQnO1xyXG5cclxuXHJcbmNvbnN0IGxheW91dD17XHJcblx0bGFiZWxDb2w6e3NwYW46OH0sXHJcblx0d3JhcHBlckNvbDp7c3BhbjoxNn0sXHJcbn07XHJcblxyXG5jb25zdCB0YWlsTGF5b3V0PXtcclxuXHR3cmFwcGVyQ29sOntvZmZzZXQ6OCxzcGFuOjE2fSxcclxufTtcclxuXHJcblxyXG5jb25zdCBvbkZpbmlzaD12YWx1ZXM9PntcclxuXHRjb25zb2xlLmxvZygnU3VjY2VzcycsdmFsdWVzKTtcclxufTtcclxuXHJcbmNvbnN0IG9uRmluaXNoRmFpbGVkPWVycm9ySW5mbz0+e1xyXG5cdGNvbnNvbGUubG9nKCdGYWlsZWQnLCBlcnJvckluZm8pO1xyXG59O1xyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5mdW5jdGlvbiBQYXJhRm9ybSgpIHtcclxuICByZXR1cm4gPD5cclxuICBcdFxyXG4gIFx0PEZvcm1cclxuICBcdFx0ey4uLmxheW91dH1cclxuICBcdFx0bmFtZT1cImJhc2ljXCJcclxuICBcdFx0aW5pdGlhbFZhbHVlcz17e3JlbWVtYmVyOnRydWV9fVxyXG4gIFx0XHRvbkZpbmlzaD17b25GaW5pc2h9XHJcbiAgXHRcdG9uRmluaXNoRmFpbGVkPXtvbkZpbmlzaEZhaWxlZH1cclxuICBcdD5cclxuICBcdFx0PEZvcm0uSXRlbVxyXG4gIFx0XHRcdGxhYmVsPVwiVXNlcm5hbWVcIlxyXG4gIFx0XHRcdG5hbWU9XCJ1c2VybmFtZVwiXHJcbiAgXHRcdFx0cnVsZXM9e1t7cmVxdWlyZWQ6dHJ1ZSwgbWVzc2FnZTonUGxlYXNlIGlucHV0IHlvdXIgdXNlcm5hbWUhJ31dfVxyXG4gIFx0XHQ+XHJcbiAgXHRcdFx0PElucHV0Lz5cclxuICBcdFx0PC9Gb3JtLkl0ZW0+XHJcblx0XHRcclxuXHJcbiAgXHRcdDxGb3JtLkl0ZW1cclxuICBcdFx0XHRsYWJlbD1cIlBhc3N3b3JkXCJcclxuICBcdFx0XHRuYW1lPVwicGFzc3dvcmRcIlxyXG4gIFx0XHRcdHJ1bGVzPXtbe3JlcXVpcmVkOnRydWUsbWVzc2FnZToncGxlYXNlIGlucHV0IHlvdXIgcGFzc3dvcmQhJ31dfVxyXG4gIFx0XHQ+XHJcbiAgXHRcdFx0PElucHV0LlBhc3N3b3JkLz5cclxuXHJcbiAgXHRcdDwvRm9ybS5JdGVtPlxyXG5cclxuXHJcbiAgXHRcdDxGb3JtLkl0ZW0gey4uLnRhaWxMYXlvdXR9PlxyXG4gIFx0XHRcdDxCdXR0b24gdHlwZT1cInByaW1hcnlcIiBodG1sVHlwZT1cInN1Ym1pdFwiPlxyXG4gIFx0XHRcdFx0U3VibWl0XHJcbiAgXHRcdFx0PC9CdXR0b24+XHJcbiAgXHRcdDwvRm9ybS5JdGVtPlxyXG5cclxuICBcdDwvRm9ybT5cclxuXHJcbiAgPC8+XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFBhcmFGb3JtOyJdLCJzb3VyY2VSb290IjoiIn0=