webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");



var _jsxFileName = "G:\\practicasTutoriales\\next\\practicaFormularios\\pages\\index.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }



var layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
};
var tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
};

var onFinish = function onFinish(values) {
  console.log('Success', values);
};

var onFinishFailed = function onFinishFailed(errorInfo) {
  console.log('Failed', errorInfo);
};

function ParaForm() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Row"], {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        span: 24,
        style: {
          backgroundColor: 'orange'
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"], {
          name: "basic",
          initialValues: {
            remember: true
          },
          onFinish: onFinish,
          onFinishFailed: onFinishFailed,
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
            label: "Email",
            name: "email",
            rules: [{
              type: 'email',
              required: true,
              message: 'Please input your username!'
            }],
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"], {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 47,
              columnNumber: 22
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 42,
            columnNumber: 19
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
            label: "Password",
            name: "password",
            rules: [{
              required: true,
              message: 'please input your password!'
            }],
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"].Password, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 56,
              columnNumber: 22
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 51,
            columnNumber: 18
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, _objectSpread(_objectSpread({}, tailLayout), {}, {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Button"], {
              type: "primary",
              htmlType: "submit",
              children: "Submit"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 62,
              columnNumber: 22
            }, this)
          }), void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 61,
            columnNumber: 18
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 35,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 5
    }, this)
  }, void 0, false);
}

_c = ParaForm;
/* harmony default export */ __webpack_exports__["default"] = (ParaForm);

var _c;

$RefreshReg$(_c, "ParaForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsibGF5b3V0IiwibGFiZWxDb2wiLCJzcGFuIiwid3JhcHBlckNvbCIsInRhaWxMYXlvdXQiLCJvZmZzZXQiLCJvbkZpbmlzaCIsInZhbHVlcyIsImNvbnNvbGUiLCJsb2ciLCJvbkZpbmlzaEZhaWxlZCIsImVycm9ySW5mbyIsIlBhcmFGb3JtIiwiYmFja2dyb3VuZENvbG9yIiwicmVtZW1iZXIiLCJ0eXBlIiwicmVxdWlyZWQiLCJtZXNzYWdlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBSUEsSUFBTUEsTUFBTSxHQUFDO0FBQ1pDLFVBQVEsRUFBQztBQUFDQyxRQUFJLEVBQUM7QUFBTixHQURHO0FBRVpDLFlBQVUsRUFBQztBQUFDRCxRQUFJLEVBQUM7QUFBTjtBQUZDLENBQWI7QUFLQSxJQUFNRSxVQUFVLEdBQUM7QUFDaEJELFlBQVUsRUFBQztBQUFDRSxVQUFNLEVBQUMsQ0FBUjtBQUFVSCxRQUFJLEVBQUM7QUFBZjtBQURLLENBQWpCOztBQUtBLElBQU1JLFFBQVEsR0FBQyxTQUFUQSxRQUFTLENBQUFDLE1BQU0sRUFBRTtBQUN0QkMsU0FBTyxDQUFDQyxHQUFSLENBQVksU0FBWixFQUFzQkYsTUFBdEI7QUFDQSxDQUZEOztBQUlBLElBQU1HLGNBQWMsR0FBQyxTQUFmQSxjQUFlLENBQUFDLFNBQVMsRUFBRTtBQUMvQkgsU0FBTyxDQUFDQyxHQUFSLENBQVksUUFBWixFQUFzQkUsU0FBdEI7QUFDQSxDQUZEOztBQVNBLFNBQVNDLFFBQVQsR0FBb0I7QUFDbEIsc0JBQU87QUFBQSwyQkFDTCxxRUFBQyx3Q0FBRDtBQUFBLDZCQUNJLHFFQUFDLHdDQUFEO0FBQUssWUFBSSxFQUFFLEVBQVg7QUFBZSxhQUFLLEVBQUU7QUFBQ0MseUJBQWUsRUFBQztBQUFqQixTQUF0QjtBQUFBLCtCQUVJLHFFQUFDLHlDQUFEO0FBRUUsY0FBSSxFQUFDLE9BRlA7QUFHRSx1QkFBYSxFQUFFO0FBQUNDLG9CQUFRLEVBQUM7QUFBVixXQUhqQjtBQUlFLGtCQUFRLEVBQUVSLFFBSlo7QUFLRSx3QkFBYyxFQUFFSSxjQUxsQjtBQUFBLGtDQU9NLHFFQUFDLHlDQUFELENBQU0sSUFBTjtBQUNHLGlCQUFLLEVBQUMsT0FEVDtBQUVHLGdCQUFJLEVBQUMsT0FGUjtBQUdHLGlCQUFLLEVBQUUsQ0FBQztBQUFDSyxrQkFBSSxFQUFDLE9BQU47QUFBZUMsc0JBQVEsRUFBQyxJQUF4QjtBQUE4QkMscUJBQU8sRUFBQztBQUF0QyxhQUFELENBSFY7QUFBQSxtQ0FLRyxxRUFBQywwQ0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEg7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFQTixlQWdCSyxxRUFBQyx5Q0FBRCxDQUFNLElBQU47QUFDSSxpQkFBSyxFQUFDLFVBRFY7QUFFSSxnQkFBSSxFQUFDLFVBRlQ7QUFHSSxpQkFBSyxFQUFFLENBQUM7QUFBQ0Qsc0JBQVEsRUFBQyxJQUFWO0FBQWVDLHFCQUFPLEVBQUM7QUFBdkIsYUFBRCxDQUhYO0FBQUEsbUNBS0kscUVBQUMsMENBQUQsQ0FBTyxRQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFMSjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQWhCTCxlQTBCSyxxRUFBQyx5Q0FBRCxDQUFNLElBQU4sa0NBQWViLFVBQWY7QUFBQSxtQ0FDSSxxRUFBQywyQ0FBRDtBQUFRLGtCQUFJLEVBQUMsU0FBYjtBQUF1QixzQkFBUSxFQUFDLFFBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkExQkw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESyxtQkFBUDtBQXlDRDs7S0ExQ1FRLFE7QUE0Q01BLHVFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4LjdjNjNkMDY5NDc3NGI2M2MzNjdmLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1JvdywgQ29sfSBmcm9tICdhbnRkJztcclxuXHJcbmltcG9ydCB7Rm9ybSwgSW5wdXQsIEJ1dHRvbiwgQ2hlY2tib3ggfSBmcm9tICdhbnRkJztcclxuXHJcblxyXG5cclxuY29uc3QgbGF5b3V0PXtcclxuXHRsYWJlbENvbDp7c3Bhbjo4fSxcclxuXHR3cmFwcGVyQ29sOntzcGFuOjE2fSxcclxufTtcclxuXHJcbmNvbnN0IHRhaWxMYXlvdXQ9e1xyXG5cdHdyYXBwZXJDb2w6e29mZnNldDo4LHNwYW46MTZ9LFxyXG59O1xyXG5cclxuXHJcbmNvbnN0IG9uRmluaXNoPXZhbHVlcz0+e1xyXG5cdGNvbnNvbGUubG9nKCdTdWNjZXNzJyx2YWx1ZXMpO1xyXG59O1xyXG5cclxuY29uc3Qgb25GaW5pc2hGYWlsZWQ9ZXJyb3JJbmZvPT57XHJcblx0Y29uc29sZS5sb2coJ0ZhaWxlZCcsIGVycm9ySW5mbyk7XHJcbn07XHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbmZ1bmN0aW9uIFBhcmFGb3JtKCkge1xyXG4gIHJldHVybiA8PlxyXG4gICAgPFJvdz5cclxuICAgICAgICA8Q29sIHNwYW49ezI0fSBzdHlsZT17e2JhY2tncm91bmRDb2xvcjonb3JhbmdlJ319PlxyXG5cclxuICAgICAgICAgICAgPEZvcm1cclxuICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIG5hbWU9XCJiYXNpY1wiXHJcbiAgICAgICAgICAgICAgaW5pdGlhbFZhbHVlcz17e3JlbWVtYmVyOnRydWV9fVxyXG4gICAgICAgICAgICAgIG9uRmluaXNoPXtvbkZpbmlzaH1cclxuICAgICAgICAgICAgICBvbkZpbmlzaEZhaWxlZD17b25GaW5pc2hGYWlsZWR9XHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICA8Rm9ybS5JdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgIGxhYmVsPVwiRW1haWxcIlxyXG4gICAgICAgICAgICAgICAgICAgICBuYW1lPVwiZW1haWxcIlxyXG4gICAgICAgICAgICAgICAgICAgICBydWxlcz17W3t0eXBlOidlbWFpbCcsIHJlcXVpcmVkOnRydWUsIG1lc3NhZ2U6J1BsZWFzZSBpbnB1dCB5b3VyIHVzZXJuYW1lISd9XX1cclxuICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICA8SW5wdXQvPlxyXG4gICAgICAgICAgICAgICAgIDwvRm9ybS5JdGVtPlxyXG5cclxuXHJcbiAgICAgICAgICAgICAgICAgPEZvcm0uSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgICBsYWJlbD1cIlBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICAgICAgICAgbmFtZT1cInBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICAgICAgICAgcnVsZXM9e1t7cmVxdWlyZWQ6dHJ1ZSxtZXNzYWdlOidwbGVhc2UgaW5wdXQgeW91ciBwYXNzd29yZCEnfV19XHJcbiAgICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgPElucHV0LlBhc3N3b3JkLz5cclxuXHJcbiAgICAgICAgICAgICAgICAgPC9Gb3JtLkl0ZW0+XHJcblxyXG5cclxuICAgICAgICAgICAgICAgICA8Rm9ybS5JdGVtIHsuLi50YWlsTGF5b3V0fT5cclxuICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiB0eXBlPVwicHJpbWFyeVwiIGh0bWxUeXBlPVwic3VibWl0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFN1Ym1pdFxyXG4gICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9Gb3JtLkl0ZW0+XHJcblxyXG4gICAgICAgICAgICA8L0Zvcm0+XHJcbiAgICAgICAgPC9Db2w+XHJcbiAgICA8L1Jvdz4gIFxyXG5cclxuICA8Lz5cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUGFyYUZvcm07Il0sInNvdXJjZVJvb3QiOiIifQ==