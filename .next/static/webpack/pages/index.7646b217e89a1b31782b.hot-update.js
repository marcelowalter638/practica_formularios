webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");



var _jsxFileName = "G:\\practicasTutoriales\\next\\practicaFormularios\\pages\\index.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }



var layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
};
var tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
};

var onFinish = function onFinish(values) {
  console.log('Success', values);
};

var onFinishFailed = function onFinishFailed(errorInfo) {
  console.log('Failed', errorInfo);
};

function ParaForm() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Row"], {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        span: 24,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"], _objectSpread(_objectSpread({}, layout), {}, {
          name: "basic",
          initialValues: {
            remember: true
          },
          onFinish: onFinish,
          onFinishFailed: onFinishFailed,
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
            label: "Email",
            name: "email",
            rules: [{
              type: 'email',
              required: true,
              message: 'Please input your username!'
            }],
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"], {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 47,
              columnNumber: 22
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 42,
            columnNumber: 19
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
            label: "Password",
            name: "password",
            rules: [{
              required: true,
              message: 'please input your password!'
            }],
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"].Password, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 56,
              columnNumber: 22
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 51,
            columnNumber: 18
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, _objectSpread(_objectSpread({}, tailLayout), {}, {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Button"], {
              type: "primary",
              htmlType: "submit",
              children: "Submit"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 62,
              columnNumber: 22
            }, this)
          }), void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 61,
            columnNumber: 18
          }, this)]
        }), void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 35,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 5
    }, this)
  }, void 0, false);
}

_c = ParaForm;
/* harmony default export */ __webpack_exports__["default"] = (ParaForm);

var _c;

$RefreshReg$(_c, "ParaForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsibGF5b3V0IiwibGFiZWxDb2wiLCJzcGFuIiwid3JhcHBlckNvbCIsInRhaWxMYXlvdXQiLCJvZmZzZXQiLCJvbkZpbmlzaCIsInZhbHVlcyIsImNvbnNvbGUiLCJsb2ciLCJvbkZpbmlzaEZhaWxlZCIsImVycm9ySW5mbyIsIlBhcmFGb3JtIiwicmVtZW1iZXIiLCJ0eXBlIiwicmVxdWlyZWQiLCJtZXNzYWdlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBSUEsSUFBTUEsTUFBTSxHQUFDO0FBQ1pDLFVBQVEsRUFBQztBQUFDQyxRQUFJLEVBQUM7QUFBTixHQURHO0FBRVpDLFlBQVUsRUFBQztBQUFDRCxRQUFJLEVBQUM7QUFBTjtBQUZDLENBQWI7QUFLQSxJQUFNRSxVQUFVLEdBQUM7QUFDaEJELFlBQVUsRUFBQztBQUFDRSxVQUFNLEVBQUMsQ0FBUjtBQUFVSCxRQUFJLEVBQUM7QUFBZjtBQURLLENBQWpCOztBQUtBLElBQU1JLFFBQVEsR0FBQyxTQUFUQSxRQUFTLENBQUFDLE1BQU0sRUFBRTtBQUN0QkMsU0FBTyxDQUFDQyxHQUFSLENBQVksU0FBWixFQUFzQkYsTUFBdEI7QUFDQSxDQUZEOztBQUlBLElBQU1HLGNBQWMsR0FBQyxTQUFmQSxjQUFlLENBQUFDLFNBQVMsRUFBRTtBQUMvQkgsU0FBTyxDQUFDQyxHQUFSLENBQVksUUFBWixFQUFzQkUsU0FBdEI7QUFDQSxDQUZEOztBQVNBLFNBQVNDLFFBQVQsR0FBb0I7QUFDbEIsc0JBQU87QUFBQSwyQkFDTCxxRUFBQyx3Q0FBRDtBQUFBLDZCQUNJLHFFQUFDLHdDQUFEO0FBQUssWUFBSSxFQUFFLEVBQVg7QUFBQSwrQkFFSSxxRUFBQyx5Q0FBRCxrQ0FDTVosTUFETjtBQUVFLGNBQUksRUFBQyxPQUZQO0FBR0UsdUJBQWEsRUFBRTtBQUFDYSxvQkFBUSxFQUFDO0FBQVYsV0FIakI7QUFJRSxrQkFBUSxFQUFFUCxRQUpaO0FBS0Usd0JBQWMsRUFBRUksY0FMbEI7QUFBQSxrQ0FPTSxxRUFBQyx5Q0FBRCxDQUFNLElBQU47QUFDRyxpQkFBSyxFQUFDLE9BRFQ7QUFFRyxnQkFBSSxFQUFDLE9BRlI7QUFHRyxpQkFBSyxFQUFFLENBQUM7QUFBQ0ksa0JBQUksRUFBQyxPQUFOO0FBQWVDLHNCQUFRLEVBQUMsSUFBeEI7QUFBOEJDLHFCQUFPLEVBQUM7QUFBdEMsYUFBRCxDQUhWO0FBQUEsbUNBS0cscUVBQUMsMENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxIO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBUE4sZUFnQksscUVBQUMseUNBQUQsQ0FBTSxJQUFOO0FBQ0ksaUJBQUssRUFBQyxVQURWO0FBRUksZ0JBQUksRUFBQyxVQUZUO0FBR0ksaUJBQUssRUFBRSxDQUFDO0FBQUNELHNCQUFRLEVBQUMsSUFBVjtBQUFlQyxxQkFBTyxFQUFDO0FBQXZCLGFBQUQsQ0FIWDtBQUFBLG1DQUtJLHFFQUFDLDBDQUFELENBQU8sUUFBUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFoQkwsZUEwQksscUVBQUMseUNBQUQsQ0FBTSxJQUFOLGtDQUFlWixVQUFmO0FBQUEsbUNBQ0kscUVBQUMsMkNBQUQ7QUFBUSxrQkFBSSxFQUFDLFNBQWI7QUFBdUIsc0JBQVEsRUFBQyxRQUFoQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBMUJMO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREssbUJBQVA7QUF5Q0Q7O0tBMUNRUSxRO0FBNENNQSx1RUFBZiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC43NjQ2YjIxN2U4OWExYjMxNzgyYi5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSb3csIENvbH0gZnJvbSAnYW50ZCc7XHJcblxyXG5pbXBvcnQge0Zvcm0sIElucHV0LCBCdXR0b24sIENoZWNrYm94IH0gZnJvbSAnYW50ZCc7XHJcblxyXG5cclxuXHJcbmNvbnN0IGxheW91dD17XHJcblx0bGFiZWxDb2w6e3NwYW46OH0sXHJcblx0d3JhcHBlckNvbDp7c3BhbjoxNn0sXHJcbn07XHJcblxyXG5jb25zdCB0YWlsTGF5b3V0PXtcclxuXHR3cmFwcGVyQ29sOntvZmZzZXQ6OCxzcGFuOjE2fSxcclxufTtcclxuXHJcblxyXG5jb25zdCBvbkZpbmlzaD12YWx1ZXM9PntcclxuXHRjb25zb2xlLmxvZygnU3VjY2VzcycsdmFsdWVzKTtcclxufTtcclxuXHJcbmNvbnN0IG9uRmluaXNoRmFpbGVkPWVycm9ySW5mbz0+e1xyXG5cdGNvbnNvbGUubG9nKCdGYWlsZWQnLCBlcnJvckluZm8pO1xyXG59O1xyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5mdW5jdGlvbiBQYXJhRm9ybSgpIHtcclxuICByZXR1cm4gPD5cclxuICAgIDxSb3c+XHJcbiAgICAgICAgPENvbCBzcGFuPXsyNH0+XHJcblxyXG4gICAgICAgICAgICA8Rm9ybVxyXG4gICAgICAgICAgICAgIHsuLi5sYXlvdXR9XHJcbiAgICAgICAgICAgICAgbmFtZT1cImJhc2ljXCJcclxuICAgICAgICAgICAgICBpbml0aWFsVmFsdWVzPXt7cmVtZW1iZXI6dHJ1ZX19XHJcbiAgICAgICAgICAgICAgb25GaW5pc2g9e29uRmluaXNofVxyXG4gICAgICAgICAgICAgIG9uRmluaXNoRmFpbGVkPXtvbkZpbmlzaEZhaWxlZH1cclxuICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgIDxGb3JtLkl0ZW1cclxuICAgICAgICAgICAgICAgICAgICAgbGFiZWw9XCJFbWFpbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJlbWFpbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgIHJ1bGVzPXtbe3R5cGU6J2VtYWlsJywgcmVxdWlyZWQ6dHJ1ZSwgbWVzc2FnZTonUGxlYXNlIGlucHV0IHlvdXIgdXNlcm5hbWUhJ31dfVxyXG4gICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgIDxJbnB1dC8+XHJcbiAgICAgICAgICAgICAgICAgPC9Gb3JtLkl0ZW0+XHJcblxyXG5cclxuICAgICAgICAgICAgICAgICA8Rm9ybS5JdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgIGxhYmVsPVwiUGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgICBuYW1lPVwicGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgICBydWxlcz17W3tyZXF1aXJlZDp0cnVlLG1lc3NhZ2U6J3BsZWFzZSBpbnB1dCB5b3VyIHBhc3N3b3JkISd9XX1cclxuICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICA8SW5wdXQuUGFzc3dvcmQvPlxyXG5cclxuICAgICAgICAgICAgICAgICA8L0Zvcm0uSXRlbT5cclxuXHJcblxyXG4gICAgICAgICAgICAgICAgIDxGb3JtLkl0ZW0gey4uLnRhaWxMYXlvdXR9PlxyXG4gICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIHR5cGU9XCJwcmltYXJ5XCIgaHRtbFR5cGU9XCJzdWJtaXRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgU3VibWl0XHJcbiAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L0Zvcm0uSXRlbT5cclxuXHJcbiAgICAgICAgICAgIDwvRm9ybT5cclxuICAgICAgICA8L0NvbD5cclxuICAgIDwvUm93PiAgXHJcblxyXG4gIDwvPlxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBQYXJhRm9ybTsiXSwic291cmNlUm9vdCI6IiJ9