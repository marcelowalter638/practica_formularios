webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");



var _jsxFileName = "G:\\practicasTutoriales\\next\\practicaFormularios\\pages\\index.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }


var layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
};
var tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
};

var onFinish = function onFinish(values) {
  console.log('Success', values);
};

var onFinishFailed = function onFinishFailed(errorInfo) {
  console.log('Failed', errorInfo);
};

function ParaForm() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"], _objectSpread(_objectSpread({}, layout), {}, {
      name: "basic",
      initialValues: {
        remember: true
      },
      onFinish: onFinish,
      onFinishFailed: onFinishFailed,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
        label: "Email",
        name: "email",
        rules: [{
          type: 'email',
          required: true,
          message: 'Please input your username!'
        }],
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"], {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 42,
          columnNumber: 6
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 5
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
        label: "Password",
        name: "password",
        rules: [{
          required: true,
          message: 'please input your password!'
        }],
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Input"].Password, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 51,
          columnNumber: 6
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 46,
        columnNumber: 5
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, _objectSpread(_objectSpread({}, tailLayout), {}, {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])(antd__WEBPACK_IMPORTED_MODULE_2__["Button"], {
          type: "primary",
          htmlType: "submit",
          children: "Submit"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 57,
          columnNumber: 6
        }, this)
      }), void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 5
      }, this)]
    }), void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 4
    }, this)
  }, void 0, false);
}

_c = ParaForm;
/* harmony default export */ __webpack_exports__["default"] = (ParaForm);

var _c;

$RefreshReg$(_c, "ParaForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsibGF5b3V0IiwibGFiZWxDb2wiLCJzcGFuIiwid3JhcHBlckNvbCIsInRhaWxMYXlvdXQiLCJvZmZzZXQiLCJvbkZpbmlzaCIsInZhbHVlcyIsImNvbnNvbGUiLCJsb2ciLCJvbkZpbmlzaEZhaWxlZCIsImVycm9ySW5mbyIsIlBhcmFGb3JtIiwicmVtZW1iZXIiLCJ0eXBlIiwicmVxdWlyZWQiLCJtZXNzYWdlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUdBLElBQU1BLE1BQU0sR0FBQztBQUNaQyxVQUFRLEVBQUM7QUFBQ0MsUUFBSSxFQUFDO0FBQU4sR0FERztBQUVaQyxZQUFVLEVBQUM7QUFBQ0QsUUFBSSxFQUFDO0FBQU47QUFGQyxDQUFiO0FBS0EsSUFBTUUsVUFBVSxHQUFDO0FBQ2hCRCxZQUFVLEVBQUM7QUFBQ0UsVUFBTSxFQUFDLENBQVI7QUFBVUgsUUFBSSxFQUFDO0FBQWY7QUFESyxDQUFqQjs7QUFLQSxJQUFNSSxRQUFRLEdBQUMsU0FBVEEsUUFBUyxDQUFBQyxNQUFNLEVBQUU7QUFDdEJDLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLFNBQVosRUFBc0JGLE1BQXRCO0FBQ0EsQ0FGRDs7QUFJQSxJQUFNRyxjQUFjLEdBQUMsU0FBZkEsY0FBZSxDQUFBQyxTQUFTLEVBQUU7QUFDL0JILFNBQU8sQ0FBQ0MsR0FBUixDQUFZLFFBQVosRUFBc0JFLFNBQXRCO0FBQ0EsQ0FGRDs7QUFTQSxTQUFTQyxRQUFULEdBQW9CO0FBQ2xCLHNCQUFPO0FBQUEsMkJBRU4scUVBQUMseUNBQUQsa0NBQ0taLE1BREw7QUFFQyxVQUFJLEVBQUMsT0FGTjtBQUdDLG1CQUFhLEVBQUU7QUFBQ2EsZ0JBQVEsRUFBQztBQUFWLE9BSGhCO0FBSUMsY0FBUSxFQUFFUCxRQUpYO0FBS0Msb0JBQWMsRUFBRUksY0FMakI7QUFBQSw4QkFPQyxxRUFBQyx5Q0FBRCxDQUFNLElBQU47QUFDQyxhQUFLLEVBQUMsT0FEUDtBQUVDLFlBQUksRUFBQyxPQUZOO0FBR0MsYUFBSyxFQUFFLENBQUM7QUFBQ0ksY0FBSSxFQUFDLE9BQU47QUFBZUMsa0JBQVEsRUFBQyxJQUF4QjtBQUE4QkMsaUJBQU8sRUFBQztBQUF0QyxTQUFELENBSFI7QUFBQSwrQkFLQyxxRUFBQywwQ0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVBELGVBZ0JDLHFFQUFDLHlDQUFELENBQU0sSUFBTjtBQUNDLGFBQUssRUFBQyxVQURQO0FBRUMsWUFBSSxFQUFDLFVBRk47QUFHQyxhQUFLLEVBQUUsQ0FBQztBQUFDRCxrQkFBUSxFQUFDLElBQVY7QUFBZUMsaUJBQU8sRUFBQztBQUF2QixTQUFELENBSFI7QUFBQSwrQkFLQyxxRUFBQywwQ0FBRCxDQUFPLFFBQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxEO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FoQkQsZUEwQkMscUVBQUMseUNBQUQsQ0FBTSxJQUFOLGtDQUFlWixVQUFmO0FBQUEsK0JBQ0MscUVBQUMsMkNBQUQ7QUFBUSxjQUFJLEVBQUMsU0FBYjtBQUF1QixrQkFBUSxFQUFDLFFBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQTFCRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGTSxtQkFBUDtBQXFDRDs7S0F0Q1FRLFE7QUF3Q01BLHVFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4LmZkZDQxNDMwMzRmNzJkMDUwYzU2LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0Zvcm0sIElucHV0LCBCdXR0b24sIENoZWNrYm94IH0gZnJvbSAnYW50ZCc7XHJcblxyXG5cclxuY29uc3QgbGF5b3V0PXtcclxuXHRsYWJlbENvbDp7c3Bhbjo4fSxcclxuXHR3cmFwcGVyQ29sOntzcGFuOjE2fSxcclxufTtcclxuXHJcbmNvbnN0IHRhaWxMYXlvdXQ9e1xyXG5cdHdyYXBwZXJDb2w6e29mZnNldDo4LHNwYW46MTZ9LFxyXG59O1xyXG5cclxuXHJcbmNvbnN0IG9uRmluaXNoPXZhbHVlcz0+e1xyXG5cdGNvbnNvbGUubG9nKCdTdWNjZXNzJyx2YWx1ZXMpO1xyXG59O1xyXG5cclxuY29uc3Qgb25GaW5pc2hGYWlsZWQ9ZXJyb3JJbmZvPT57XHJcblx0Y29uc29sZS5sb2coJ0ZhaWxlZCcsIGVycm9ySW5mbyk7XHJcbn07XHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbmZ1bmN0aW9uIFBhcmFGb3JtKCkge1xyXG4gIHJldHVybiA8PlxyXG4gIFx0XHJcbiAgXHQ8Rm9ybVxyXG4gIFx0XHR7Li4ubGF5b3V0fVxyXG4gIFx0XHRuYW1lPVwiYmFzaWNcIlxyXG4gIFx0XHRpbml0aWFsVmFsdWVzPXt7cmVtZW1iZXI6dHJ1ZX19XHJcbiAgXHRcdG9uRmluaXNoPXtvbkZpbmlzaH1cclxuICBcdFx0b25GaW5pc2hGYWlsZWQ9e29uRmluaXNoRmFpbGVkfVxyXG4gIFx0PlxyXG4gIFx0XHQ8Rm9ybS5JdGVtXHJcbiAgXHRcdFx0bGFiZWw9XCJFbWFpbFwiXHJcbiAgXHRcdFx0bmFtZT1cImVtYWlsXCJcclxuICBcdFx0XHRydWxlcz17W3t0eXBlOidlbWFpbCcsIHJlcXVpcmVkOnRydWUsIG1lc3NhZ2U6J1BsZWFzZSBpbnB1dCB5b3VyIHVzZXJuYW1lISd9XX1cclxuICBcdFx0PlxyXG4gIFx0XHRcdDxJbnB1dC8+XHJcbiAgXHRcdDwvRm9ybS5JdGVtPlxyXG5cdFx0XHJcblxyXG4gIFx0XHQ8Rm9ybS5JdGVtXHJcbiAgXHRcdFx0bGFiZWw9XCJQYXNzd29yZFwiXHJcbiAgXHRcdFx0bmFtZT1cInBhc3N3b3JkXCJcclxuICBcdFx0XHRydWxlcz17W3tyZXF1aXJlZDp0cnVlLG1lc3NhZ2U6J3BsZWFzZSBpbnB1dCB5b3VyIHBhc3N3b3JkISd9XX1cclxuICBcdFx0PlxyXG4gIFx0XHRcdDxJbnB1dC5QYXNzd29yZC8+XHJcblxyXG4gIFx0XHQ8L0Zvcm0uSXRlbT5cclxuXHJcblxyXG4gIFx0XHQ8Rm9ybS5JdGVtIHsuLi50YWlsTGF5b3V0fT5cclxuICBcdFx0XHQ8QnV0dG9uIHR5cGU9XCJwcmltYXJ5XCIgaHRtbFR5cGU9XCJzdWJtaXRcIj5cclxuICBcdFx0XHRcdFN1Ym1pdFxyXG4gIFx0XHRcdDwvQnV0dG9uPlxyXG4gIFx0XHQ8L0Zvcm0uSXRlbT5cclxuXHJcbiAgXHQ8L0Zvcm0+XHJcblxyXG4gIDwvPlxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBQYXJhRm9ybTsiXSwic291cmNlUm9vdCI6IiJ9